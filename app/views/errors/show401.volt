{{ content() }}
<div class="jumbotron">
    <h1>Not authorized</h1>
    <p>You are not authorized to view this page</p>
    <p>{{ link_to('member', 'search', 'class': 'btn btn-primary') }}</p>
</div>
<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class WorkoutController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for Workout
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Workout', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $workout = Workout::find($parameters);
        if (count($workout) == 0) {
            $this->flash->notice("The search did not find any Workout");

            $this->dispatcher->forward([
                "controller" => "Workout",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $workout,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a Workout
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $workout = Workout::findFirstByid($id);
            if (!$workout) {
                $this->flash->error("Workout was not found");

                $this->dispatcher->forward([
                    'controller' => "Workout",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $workout->getId();

            $this->tag->setDefault("id", $workout->getId());
            $this->tag->setDefault("claggss", $workout->getClass());
            $this->tag->setDefault("studio", $workout->getStudio());
            $this->tag->setDefault("weights", $workout->getWeights());
            
        }
    }

    /**
     * Creates a new Workout
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "Workout",
                'action' => 'index'
            ]);

            return;
        }

        $workout = new Workout();
        $workout->setclass($this->request->getPost("class"));
        $workout->setstudio($this->request->getPost("studio"));
        $workout->setweights($this->request->getPost("weights"));
        

        if (!$workout->save()) {
            foreach ($workout->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "Workout",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("Workout was created successfully");

        $this->dispatcher->forward([
            'controller' => "Workout",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a Workout edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "Workout",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $workout = Workout::findFirstByid($id);

        if (!$workout) {
            $this->flash->error("Workout does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "Workout",
                'action' => 'index'
            ]);

            return;
        }

        $workout->setclass($this->request->getPost("class"));
        $workout->setstudio($this->request->getPost("studio"));
        $workout->setweights($this->request->getPost("weights"));
        

        if (!$workout->save()) {

            foreach ($workout->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "Workout",
                'action' => 'edit',
                'params' => [$workout->getId()]
            ]);

            return;
        }

        $this->flash->success("Workout was updated successfully");

        $this->dispatcher->forward([
            'controller' => "Workout",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a Workout
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $workout = Workout::findFirstByid($id);
        if (!$workout) {
            $this->flash->error("Workout was not found");

            $this->dispatcher->forward([
                'controller' => "Workout",
                'action' => 'index'
            ]);

            return;
        }

        if (!$workout->delete()) {

            foreach ($workout->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "Workout",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("Workout was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "Workout",
            'action' => "index"
        ]);
    }

}

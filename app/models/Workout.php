<?php

class Workout extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(column="class", type="string", length=30, nullable=true)
     */
    protected $class;

    /**
     *
     * @var integer
     * @Column(column="studio", type="integer", length=1, nullable=true)
     */
    protected $studio;

    /**
     *
     * @var integer
     * @Column(column="weights", type="integer", length=1, nullable=true)
     */
    protected $weights;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field class
     *
     * @param string $class
     * @return $this
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Method to set the value of field studio
     *
     * @param integer $studio
     * @return $this
     */
    public function setStudio($studio)
    {
        $this->studio = $studio;

        return $this;
    }

    /**
     * Method to set the value of field weights
     *
     * @param integer $weights
     * @return $this
     */
    public function setWeights($weights)
    {
        $this->weights = $weights;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Returns the value of field studio
     *
     * @return integer
     */
    public function getStudio()
    {
        return $this->studio;
    }

    /**
     * Returns the value of field weights
     *
     * @return integer
     */
    public function getWeights()
    {
        return $this->weights;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("studio");
        $this->setSource("Workout");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'Workout';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Workout[]|Workout|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Workout|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
